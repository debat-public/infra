
##################################################
# Docker-compose
##################################################

dev_start:  dev_delete
	docker-compose -f dev/docker-compose.yaml up -d

dev_stop:
	docker-compose -f dev/docker-compose.yaml stop

dev_delete:
	docker-compose -f dev/docker-compose.yaml rm -fsv

dev_start_build: dev_delete
	docker-compose -f dev/docker-compose.yaml up --build

docker_login:
	docker login registry.gitlab.com

##################################################
# Kubernetes
##################################################


cluster_create: cluster_create_deployment cluster_create_service

cluster_delete: cluster_delete_deployment cluster_delete_service

cluster_create_deployment: cluster_create_server_deployment cluster_create_client-web_deployment

cluster_delete_deployment: cluster_delete_server_deployment cluster_delete_client-web_deployment

cluster_create_service: cluster_create_client-web_service cluster_create_server_service

cluster_delete_service: cluster_delete_client-web_service cluster_delete_server_service
	
cluster_create_client-web_service:
	kubectl apply -f client-web-service.yaml

cluster_create_server_service:
	kubectl apply -f server-service.yaml

cluster_delete_client-web_service:
	kubectl delete services/client-web-service

cluster_delete_server_service:
	kubectl delete services/server-service

cluster_create_server_deployment:
	kubectl apply -f server-deployment.yaml

cluster_create_client-web_deployment:
	kubectl apply -f client-web-deployment.yaml

cluster_delete_server_deployment:
	kubectl delete deployments/server-deployment

cluster_delete_client-web_deployment:
	kubectl delete deployments/client-web-deployment

create_registry_server_secret:
	kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=${GITLAB_USERNAME} --docker-password=${GITLAB_PASSWORD} --docker-email=${GITLAB_EMAIL}


##################################################
# Minikube lifecycle
##################################################

create_local_cluster:
	minikube version
	minikube start

delete_local_cluster:
	minikube delete

stop_local_cluster:
	minikube stop

get_local_status:
	minikube status

local_dashboard:
	minikube dashboard

ip_local_cluster:
	minikube ip

##################################################
# MacOS install Kubernetes
##################################################

macos_install:
	brew cask install minikube

macos_reinstall:
	brew cask reinstall minikube

##################################################
# Kubernetes cluster informations
##################################################

cluster_all_informations:
	kubectl cluster-info
	kubectl get pods
	kubectl get nodes
	kubectl get services
	kubectl get deployments
	kubectl describe pods
	kubectl get rs
	kubectl get secrets


#More infos pods
# kubectl get pods -o wide

# kubectl logs $POD_NAME
# kubectl exec $POD_NAME env
# kubectl exec -t client-web-deployment-cdfsfsd sh

# kubectl create deployment
# kubectl describe deployments/$POD_NAME
# kubectl scale deployments/$POD_NAME --replicas=4

# kubectl create service
# kubectl describe services/$

# #Update pod image
# kubectl set image deployments/$POD_NAME $NEW_IMAGE

